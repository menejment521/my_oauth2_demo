package uz.pdp.oauth2_demoi.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.oauth2_demoi.entity.User;

public interface UserRepo extends JpaRepository<User,Integer> {
}
