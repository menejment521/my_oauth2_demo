package uz.pdp.oauth2_demoi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OAuth2DemoiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OAuth2DemoiApplication.class, args);
    }

}
